
package exercices;

import ejb.dao.parcelle.DaoParcelle;
import ejb.entites.Parcelle;
import ejb.entites.Traitement;
import ejb.entites.TraitementEnChamp;
import java.util.Scanner;

public class Exo1 {
 
    public static void main(String[] args) {

        Scanner clavier = new Scanner(System.in);
        
        DaoParcelle daoPar=new DaoParcelle();
            
        System.out.print("\nEntrez l'dentifiant de la parcelle: ");
        
        String idParc=clavier.next();
        
        Parcelle p=daoPar.getLaParcelle(idParc);
        
        System.out.println(
                "\nParcelle appartenant  à "+
                p.getlExploitation().getNomExploitant()+ " "+
                p.getlExploitation().getAdrExploitant()+ "\n"+
                "Surface:  "+
                p.getSurface()+ " ha culture: "+
                p.getlEspeceCultivee().getNomEsp()
        );
        
        
        System.out.println("\nTraitements appliqués:\n");
        
        for( Traitement t : p.getLesTraitements()){
        
            System.out.printf(
                "%-5s %-20s %8.2f kg ",
                 t.getLeProduit().getNomprod(),
                 t.getTypeTraitement(),
                 t.quantiteAppliquee() 
            ); 
            
            if  (t instanceof TraitementEnChamp){
               TraitementEnChamp tec= (TraitementEnChamp) t;
                System.out.println(" en "+ tec.getLesPulverisations().size()+ " pulvérisations");
            }
            else System.out.println();
         }
        
         System.out.println();
        
    }
}
