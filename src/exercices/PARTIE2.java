
package exercices;

import ejb.dao.exploitation.DaoExploitation;
import ejb.dao.parcelle.DaoParcelle;
import ejb.entites.Exploitation;
import ejb.entites.Parcelle;


public class PARTIE2 {
     public static void main(String[] args) {
         
        
         DaoExploitation dao = new DaoExploitation();
         Exploitation Exp = dao.getLExploitation("exp001");
         
         Exp.surfaceTotaleDesParcelles();
         Exp.surfaceCultivéeEspece("espC1");
         Exp.afficherCultures();
         
         DaoParcelle dao2 = new DaoParcelle();
         Parcelle Par = dao2.getLaParcelle("parc01");
         
         Par.poidsProduitUtilise("prod007");
     }

}
