package ejb.entites;
import java.util.LinkedList;
import java.util.List;

public class TraitementEnChamp  extends Traitement{

    private List<Pulverisation> lesPulverisations= new LinkedList();;

    public TraitementEnChamp( ) {
        
        super();
    }

    public TraitementEnChamp(String idTrait, Produit leProduit, Parcelle laParcelle) {
       
        super(idTrait, leProduit, laParcelle);
    }
    
    // quantiteAppliquee
    // Un traitement en champ comporte plusieurs pulverisations
    // A chaque  pulvérisation le poids appliqué s'obtient
    // en multipliant le dosage de la pulverisation ( en kg par  hectare)
    // par la surface de la parcelle concernée ( en hectares ) 
    
    // la quantité à retourner est donc la somme de tous ces poids
    
    @Override
    public Float quantiteAppliquee() {
      
        Float qteProduit=0F;
        
        for(Pulverisation pulv : lesPulverisations){
           qteProduit+=pulv.getDosagePulv();
        }
        
        return qteProduit*getLaParcelle().getSurface();  
        
    }

    //<editor-fold defaultstate="collapsed" desc="Gets & Sets">
    
    public List<Pulverisation> getLesPulverisations() {
        return lesPulverisations;
    }
    
    public void setLesPulverisations(List<Pulverisation> lesPulverisations) {
        this.lesPulverisations = lesPulverisations;
    }
    //</editor-fold>
}