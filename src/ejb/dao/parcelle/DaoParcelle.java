
package ejb.dao.parcelle;

import ejb.entites.Parcelle;
import java.util.LinkedList;
import java.util.List;

import stockagemem.Donnees;

public class DaoParcelle {

    private Donnees data;

    public DaoParcelle() {
        this.data = new Donnees();
    }
    
     public Parcelle getLaParcelle(String idParc) {
        
        return data.lesParcelles.get(idParc);
    }

     public  List<Parcelle> getToutesLesParcelles() {
       
        return new LinkedList<Parcelle>(data.lesParcelles.values());
    }    
}
